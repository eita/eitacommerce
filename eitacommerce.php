<?php

add_filter( 'gettext', 'translate_cycle_edit_page' );
function translate_cycle_edit_page( $translated_text ) {
    if ( 'Comprar novamente' === $translated_text ) {
        $translated_text = 'Repetir pedido';
    }

    return $translated_text;
}

/**
 * Plugin Name: EITA Commerce
 * Plugin URI: https://gitlab.com/eita/eitacommerce/
 * Description: This plugin adds some essential missings funcionalities to Woocommerce.
 * Version: 1.0.0
 * Author: Cooperativa EITA
 * Author URI: https://eita.coop.br
 * Text Domain: eitacommerce
 */

add_action( 'wp_enqueue_scripts', 'eitacommerce_enqueue_styles' );
function eitacommerce_enqueue_styles() {
		wp_enqueue_style(
			'eitacommerce-css',
			plugins_url( 'assets/css/style.css', __FILE__ )
		);
}

/**
 * Shortcode to show all products separated by a taxonomy (currently, only category)
 */

add_shortcode( 'show_categories_and_products', 'show_categories_and_products');
function show_categories_and_products( $atts ){

    if( isset($atts['columns'] ) && $atts['columns'] ) {
        $cols = $atts['columns'];
    } else {
        $cols = 5;
    }

    if( isset($atts['taxonomy']) && $atts['taxonomy'] ) {
        $taxonomy = $atts['taxonomy'];
    } else {
        $taxonomy = 'product_cat';
    }

    $categories = get_terms( array( 'taxonomy' => $taxonomy, 'hide_empty' => true) );

    $html = "";
    foreach ($categories as $category) {

        $args = array(
            'post_type' => 'product',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                  'taxonomy' => $taxonomy,
                  'field' => 'term_id',
                  'terms' => $category->term_id
                ),
                array(
                  'taxonomy' => 'product_visibility',
                  'field' => 'name',
                  'terms' => ['outofstock', 'exclude-from-catalog'],
                  'operator' => 'NOT IN'
                )
	    ),
        );

        $posts = get_posts( $args );


        if ($posts) {
						$posts = array_map( function( $k ) { return $k->ID; }, $posts);
						$posts = implode( ",", $posts );

            $html .= '
              <h2>'.$category->name.'</h2>
							<!-- wp:woocommerce/handpicked-products {"columns":'.$cols.', "editMode":false, "orderby": "title", "products":['.$posts.']} /-->
              <!-- wp:spacer {"height":100} -->
                <div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
              <!-- /wp:spacer -->
            ';
        }
    }
    $blocks = parse_blocks( $html );
    $output = "";
    foreach( $blocks as $block ) {
        $output .= render_block( $block );
    }
    return $output;
}

/**
 * Override loop template and show quantities next to add to cart buttons
 */

add_filter( 'woocommerce_loop_add_to_cart_link', 'quantity_inputs_for_woocommerce_loop_add_to_cart_link', 10, 2 );
function quantity_inputs_for_woocommerce_loop_add_to_cart_link( $html, $product ) {

        if ( $product && $product->is_type( 'simple' ) && $product->is_purchasable() && $product->is_in_stock() && ! $product->is_sold_individually() ) {
                $html = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
                $html .= woocommerce_quantity_input( array(), $product, false );
                $html .= '<button type="submit" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
                $html .= '</form>';
        }
        return $html;
}

/**
 * Same as before, now applied to Woocommerce blocks
 */
function add_qty_selector( $html, $data, $product ) {

	if ( ! $product->is_purchasable()) {
		return $html;
	}

        $button = '<form action="' . esc_url( $product->add_to_cart_url() ) . '" class="cart" method="post" enctype="multipart/form-data">';
        $button .= woocommerce_quantity_input( array(), $product, false );
        $button .= '<button type="submit" class="button alt">' . esc_html( $product->add_to_cart_text() ) . '</button>';
        $button .= '</form>';


        $html = "<li class=\"wc-block-grid__product\">
                <a href=\"{$data->permalink}\" class=\"wc-block-grid__product-link\">
                {$data->image}
                {$data->title}
                </a>
                {$data->badge}
                {$data->price}
                {$data->rating}
                {$button}
                </li>";

        return $html;

}
add_filter( 'woocommerce_blocks_product_grid_item_html', 'add_qty_selector', 10, 3 );

/*
* Adds a dynamic menu with the terms of a taxonomy. User must add a menu item a set it class as automenu_{taxonomy_name}. Example: automenu_product_tag. This menu item must have a submenu item to be copied.
*/
function add_taxonomy_to_menu( $nav_menu, $args ){

	// for some reason, mobile menu do not set a menu class; using this while there's no better solution
	$menu_class = $args->menu_class;
	if ( ! $menu_class ){
		$menu_class = "mobile-menu";
	}

	$menu_items = wp_get_nav_menu_items( $args->menu );
	$js = "";
    if ( $menu_items ) {

        foreach ($menu_items as $menu_item) {
            if (substr($menu_item->classes[0],0,9) == 'automenu_'){

                $taxonomy = substr($menu_item->classes[0],9);
                if ( ! taxonomy_exists( $taxonomy )){ return ""; }

                $terms = get_terms( array( 'taxonomy' => $taxonomy, 'hide_empty' => 'true' ));
                $js .= '
                    <script>
                        var submenu = jQuery(".' . $menu_class . ' .' . $menu_item->classes[0].' .sub-menu");
                        var first_list_item = jQuery(".' . $menu_class .' .' . $menu_item->classes[0] . ' .sub-menu li:first");
                        var list_item = first_list_item.clone();
                ';

                foreach ($terms as $term) {

                    $args = array(
                        'post_type' 			=> 'product',
                        'posts_per_page' 	=> -1,
                        'tax_query' 			=> array(
                            'relation' 				=> 'AND',
                            array(
                                'taxonomy' 	=> $taxonomy,
                                'field' 		=> 'term_id',
                                'terms' 		=> $term->term_id
                            ),
                            array(
                                'taxonomy' 	=> 'product_visibility',
                                'field' 		=> 'name',
                                'terms' 		=> ['outofstock', 'exclude-from-catalog'],
                                'operator' 	=> 'NOT IN'
                            )
                        ),
                    );

            $posts = get_posts( $args );
                    if ( $posts ){
                        $link = get_term_link( $term );
                        $js .= '
                            var term = list_item.clone();
                            term.children("a").attr("href", "'.$link.'");
                            term.children("a").text("'.htmlspecialchars_decode($term->name).'");
                            submenu.append(term);
    ';
                    }
                }
                $js .= '
                    first_list_item.remove();
                    </script>
                ';
            }
        }
    }

	return $nav_menu . $js;
}
add_filter( 'wp_nav_menu', 'add_taxonomy_to_menu', 10, 2);


/**
 * @snippet       Edit Order Functionality @ WooCommerce My Account Page
 * @how-to        Get CustomizeWoo.com FREE
 * @sourcecode    https://businessbloomer.com/?p=91893
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 4.1
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

// ----------------
// 1. Allow Order Again for Processing Status

add_filter( 'woocommerce_valid_order_statuses_for_order_again', 'bbloomer_order_again_statuses' );

function bbloomer_order_again_statuses( $statuses ) {
    $statuses[] = 'on-hold';
    return $statuses;
}

// ----------------
// 2. Add Order Actions @ My Account

add_filter( 'woocommerce_my_account_my_orders_actions', 'bbloomer_add_edit_order_my_account_orders_actions', 50, 2 );

function bbloomer_add_edit_order_my_account_orders_actions( $actions, $order ) {
    if ( $order->has_status( 'on-hold' ) ) {
        $actions['edit-order'] = array(
            'url'  => wp_nonce_url( add_query_arg( array( 'order_again' => $order->get_id(), 'edit_order' => $order->get_id() ) ), 'woocommerce-order_again' ),
            'name' => __( 'Edit Order', 'woocommerce' )
        );
    }
    return $actions;
}

// ----------------
// 3. Detect Edit Order Action and Store in Session

add_action( 'woocommerce_cart_loaded_from_session', 'bbloomer_detect_edit_order' );

function bbloomer_detect_edit_order( $cart ) {
    if ( isset( $_GET['edit_order'], $_GET['_wpnonce'] ) && is_user_logged_in() && wp_verify_nonce( wp_unslash( $_GET['_wpnonce'] ), 'woocommerce-order_again' ) ) WC()->session->set( 'edit_order', absint( $_GET['edit_order'] ) );
}

// ----------------
// 4. Display Cart Notice re: Edited Order

add_action( 'woocommerce_before_cart', 'bbloomer_show_me_session' );

function bbloomer_show_me_session() {
    if ( ! is_cart() ) return;
    $edited = WC()->session->get('edit_order');
    if ( ! empty( $edited ) ) {
        $order = new WC_Order( $edited );
        // $credit = $order->get_total();
        // wc_print_notice( 'A credit of ' . wc_price($credit) . ' has been applied to this new order. Feel free to add products to it or change other details such as delivery date.', 'notice' );
    }
}

// ----------------
// 5. Calculate New Total if Edited Order

add_action( 'woocommerce_cart_calculate_fees', 'bbloomer_use_edit_order_total', 20, 1 );

function bbloomer_use_edit_order_total( $cart ) {

  if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

  $edited = WC()->session->get('edit_order');
  if ( ! empty( $edited ) ) {
      $order = new WC_Order( $edited );
      // $credit = -1 * $order->get_total();
      // $cart->add_fee( 'Credit', $credit );
  }

}

// ----------------
// 6. Save Order Action if New Order is Placed

add_action( 'woocommerce_checkout_update_order_meta', 'bbloomer_save_edit_order' );

function bbloomer_save_edit_order( $order_id ) {
    $edited = WC()->session->get( 'edit_order' );
    if ( ! empty( $edited ) ) {
        // update this new order
        update_post_meta( $order_id, '_edit_order', $edited );
        $neworder = new WC_Order( $order_id );
        $oldorder_edit = get_edit_post_link( $edited );
        $neworder->add_order_note( 'Order placed after editing. Old order number: <a href="' . $oldorder_edit . '">' . $edited . '</a>' );
        // cancel previous order
        $oldorder = new WC_Order( $edited );
        $neworder_edit = get_edit_post_link( $order_id );
        $oldorder->update_status( 'cancelled', 'Order cancelled after editing. New order number: <a href="' . $neworder_edit . '">' . $order_id . '</a> -' );
        WC()->session->set( 'edit_order', null );
    }
}


/** allow shop manager to add user */

function shop_manager_add_users() {
    $role = get_role( 'shop_manager' );
    $role->add_cap('list_users' );
    $role->add_cap('edit_users' );
    $role->add_cap('create_users' );
    $role->add_cap('delete_users' );
    $role->add_cap('promote_users' );
}
add_action( 'admin_init', 'shop_manager_add_users');

function eita_editable_roles( $roles ) {
    if ( $user = wp_get_current_user() ) {
		if ( in_array( 'shop_manager', $user->roles ) ) { 
			global $wp_roles;

			$roles = $wp_roles->roles;
			unset( $roles['administrator'] );
		}
    }
    return $roles;
}

add_filter( 'editable_roles', 'eita_editable_roles', 999, 1 );

/**
 * Prevent users deleting/editing users with a role outside their allowance.
 */
function eita_map_meta_cap( $caps, $cap, $user_ID, $args ) {
    if ( ( $cap === 'edit_user' || $cap === 'delete_user' ) && $args ) {
        $the_user = get_userdata( $user_ID ); // The user performing the task
        $user     = get_userdata( $args[0] ); // The user being edited/deleted
		
		if ( in_array( 'shop_manager', $the_user->roles )) {
			if ( $the_user && $user && array_diff( $user->roles, array('administrator'))) {
				$caps = array('edit_users');
			} else {
				$caps[] = 'not_allowed';
			}
		}
    }
    return $caps;
}

add_filter( 'map_meta_cap', 'eita_map_meta_cap', 10, 4 );