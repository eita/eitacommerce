# Eitacommerce

En: A set of Woocommerce enhancements by Cooperativa EITA

Pt_br: Um conjunto de melhorias e ajustes ao Wordpress/Woocommerce a partir da experiências de implementação de lojas de produtos da agricultura famililar e reforma agrária da Cooperativa EITA.

## Funcionalidades

### Exibição de todos os produtos separados por categoria (ou outra taxonomia)

Para montagem rápida de uma página inicial, você pode utilizar o shortcode `[show_categories_and_products]`. Ele irá exibir todos os produtos disponíveis (e em estoque) separados por títulos das categorias em blocos de 5 colunas. Você poderá alterar a taxonomia e o número de colunas:

`[show_categories_and_products columns='5' taxonomy='product_cat']`

### Alterar a quantidade dos produtos antes de inserir no carrinho

Esta funcionalidade insere um campo de quantidade (com botões de mais e menos) ao lado do botão de inserir no carrinho. Assim, a pessoa pode colocar a quantidade desejada no carrinho e não precisa alterar no final da compra.

### Exibição de gif loading na finalização da compra

Um grande problema de usabilidade que percebemos no Woocommerce é a falta de feedback no momento principal da compra: o checkout. Assim, incluimos um gif de loading que aparece enquanto o WC está procesando o pagamento, para que a pessoa saiba que está procesando.

### Adicionar taxonomia completa a um menu (em desenvolvimento)

Frequentemente, gostaríamos de ter um menu (ou submenu) contendo todos os termos de uma taxonomia relacionada a produtos. Pode ser a categoria, tag, ou outra taxonomia que venha a ser criada.

Para utilizar:

1. Crie um item de menu (Link Personalizado), atribua um nome e digite `#` no campo do link;

2. Para este menu, no campo "Classes de CSS" digite `automenu_{taxonomia}`, trocando `{taxonomia}` pelo nome da taxonomia desejada. Por exemplo, a classe para utilizar categorias de produto seria `automenu_product_cat`, ou para tags seria `automenu_product_tag`. Caso você não esteja vendo o campo Classes de CSS, vá em "Opções de Tela" (canto superior direito) e habilite a opção "Classes de CSS".

3. Adicione um submenu qualquer a este menu, para que o plugin possa detectar como criar novos submenus.

Agora, ao carregar o site, abaixo do menu criado você verá um submenu com todas os termos que contenham produtos visíveis.
### Permitir que um gerente de loja (woocommerce) adicione usuários

Por padrão o woocommerce não permite que gerentes de loja adicionem novos usuários. Com o plugin os gerentes conseguem criar e editar usuários que não sejam administradores.

## Configurações recomendadas

Nesta seção, recomendamos algumas configurações do Woocommerce que podem ajudar na gestão da sua loja ou grupo de consumo.

1. Woocommerce > Configurações > Produtos > Inventário: Nesta seção, recomendamos marcar a opção "Ocultar os produtos fora de estoque do catálogo", de modo que os produtos com estoque zerado ou marcados como fora de estoque não apareçam no site nem nas buscas.

1. Woocommerce > Configurações > E-mails: Nesta seção, recomendamos verificar as notificações por e-mail. Em geral, desabilitamos o envio de email na conclusão dos pedidos. Além disso, verificar os destinatários de cada email. Em Configurações > Produtos > Inventário também há uma opção de envio de email de estoque baixo onde é necessário verificar o destinatário deste email.

1. Woocommerce > Configurações > Contas e privacidade: Nesta seção é possível restringir as compras a clientes registrados (com login) ou não. Ao marcar a opção "Permitir que seus clientes efetuem pedidos sem uma conta", você permite que se façam compras sem registro do cliente. Esta opção tem a vantagem de criar menos barreiras à compra, no caso de lojas ou grupos abertos. Sem essa opção marcada, somente será possível finalizar a compra inserindo uma senha. A vantagem deste método é poder ter controle sobre quem compra. Além disso, cliente logados podem recuperar o carrinho mesmo após fechar o navegador ou se logar em outra máquina. Os dados cadastrais também ficam registrados, de modo a não necessitar que sejam reinseridos. Um loja também pode trabalhar aceitando compras de clientes com e sem login.

## Plugins Obrigatórios

1. [Woocommerce](https://br.wordpress.org/plugins/woocommerce/)

1. [WP Mail SMTP](https://br.wordpress.org/plugins/wp-mail-smtp/) (ou outro equivalente)

O Woocommerce é extremamente dependente do envio de emails. Assim, é fundamental instalar um plugin que envie emails via SMTP, que é um método mais confiável do que o padrão do Wordpress (PHP Mail).

## Plugins Recomendados

1. [WooCommerce Side Cart (Ajax)](https://br.wordpress.org/plugins/side-cart-woocommerce/)

1. [Checkout Field Editor for WooCommerce](https://br.wordpress.org/plugins/woo-checkout-field-editor-pro/)

1. [WooCommerce PDF Invoices & Packing Slips](https://br.wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/)
